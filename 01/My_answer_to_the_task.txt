1. Определить метод запроса
	- Метод запроса при поиске товаров в каталоге onliner.by - "GET"

2. Привести пример заголовков запроса
	- Заголовки запроса при поиске "notebook dell"

		GET https://catalog.onliner.by/sdapi/catalog.api/search/products?query=notebook+dell HTTP/1.1
		Host: catalog.onliner.by
		User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0
		Accept: application/json, text/javascript, */*; q=0.01
		Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
		Accept-Encoding: gzip, deflate, br
		X-Requested-With: XMLHttpRequest
		Connection: keep-alive
		Referer: https://catalog.onliner.by/sdapi/catalog/search/iframe
		Cookie: __io=c6269cc2e.24e3d23a5_1600842801138; __io_unique_12862=23; ouid=snyBEF9q7DJa2B47CKYBAg==; _ym_uid=1600842804637773758; _ym_d=1600842804; tmr_reqNum=6; tmr_lvid=b0d9291e0bdc3df70354145aa66554f6; tmr_lvidTS=1600842805520; _ym_isad=2; _ga=GA1.1.1668908287.1600842806; _gid=GA1.2.1024267528.1600842806; catalog_session=b6VcW4xu0fjeKBgDlZuiztc5eD4o29a6ecDtWLA4; _rs_n=3; _rs_uid=4f222adb-4fe9-41d9-b6d9-00bd9942f184; _gcl_au=1.1.953054261.1600842818; _ga_4Y6NQKE48G=GS1.1.1600842817.1.1.1600844683.56; _fbp=fb.1.1600842820767.725987044; __io_d=1_1815979541; __io_session_id=620af7c26.0e8d05252_1600844652476; __io_nav_state12862=%7B%22current%22%3A%22%2F%22%7D; __io_visit_12862=1; _ym_visorc_1911064=w; _gat_UA-340679-1=1; _gat_UA-340679-13=1; _gat_UA-340679-51=1; _ym_visorc_5770612=w; _ym_visorc_5770561=b; _gat_UA-340679-16=1

	- Заголовки запроса при поиске "стиральная машина"

		GET https://catalog.onliner.by/sdapi/catalog.api/search/products?query=%D1%81%D1%82%D0%B8%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F+%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D0%B0 HTTP/1.1
		Host: catalog.onliner.by
		User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0
		Accept: application/json, text/javascript, */*; q=0.01
		Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3
		Accept-Encoding: gzip, deflate, br
		X-Requested-With: XMLHttpRequest
		Connection: keep-alive
		Referer: https://catalog.onliner.by/sdapi/catalog/search/iframe
		Cookie: __io=c6269cc2e.24e3d23a5_1600842801138; __io_unique_12862=23; ouid=snyBEF9q7DJa2B47CKYBAg==; _ym_uid=1600842804637773758; _ym_d=1600842804; tmr_reqNum=6; tmr_lvid=b0d9291e0bdc3df70354145aa66554f6; tmr_lvidTS=1600842805520; _ym_isad=2; _ga=GA1.1.1668908287.1600842806; _gid=GA1.2.1024267528.1600842806; catalog_session=b6VcW4xu0fjeKBgDlZuiztc5eD4o29a6ecDtWLA4; _rs_n=3; _rs_uid=4f222adb-4fe9-41d9-b6d9-00bd9942f184; _gcl_au=1.1.953054261.1600842818; _ga_4Y6NQKE48G=GS1.1.1600842817.1.1.1600844683.56; _fbp=fb.1.1600842820767.725987044; __io_d=1_1815979541; __io_session_id=620af7c26.0e8d05252_1600844652476; __io_nav_state12862=%7B%22current%22%3A%22%2F%22%7D; __io_visit_12862=1; _ym_visorc_1911064=w; _ym_visorc_5770612=w; _ym_visorc_5770561=b

3. Привести пример заголовков ответа
	- Заголовки ответа на запрос поиска "notebook dell"

		HTTP/1.1 200 OK
		Server: nginx
		Date: Wed, 23 Sep 2020 07:05:17 GMT
		Content-Type: application/json; charset=utf-8
		Transfer-Encoding: chunked
		Connection: keep-alive
		Keep-Alive: timeout=15
		Cache-Control: no-cache, private
		ETag: W/"2c6c439d6fd670799bd6f4bcd5d790af"
		Access-Control-Allow-Origin: 
		Access-Control-Allow-Credentials: true
		Content-Encoding: gzip
		X-Frame-Options: SAMEORIGIN
		X-Content-Type-Options: nosniff
		X-XSS-Protection: 1; mode=block

	- Заголовки ответа на запрос поиска "стиральная машина"

		HTTP/1.1 200 OK
		Server: nginx
		Date: Wed, 23 Sep 2020 07:06:54 GMT
		Content-Type: application/json; charset=utf-8
		Transfer-Encoding: chunked
		Connection: keep-alive
		Keep-Alive: timeout=15
		Cache-Control: no-cache, private
		ETag: W/"5aa5648790b8185ffb0bad098b8fd086"
		Access-Control-Allow-Origin: 
		Access-Control-Allow-Credentials: true
		Content-Encoding: gzip
		X-Frame-Options: SAMEORIGIN
		X-Content-Type-Options: nosniff
		X-XSS-Protection: 1; mode=block



